<?php
/**
 * Created by PhpStorm.
 * User: pptasinski
 * Date: 25.05.17
 * Time: 21:36
 */

namespace AppBundle\Event\Subscriber;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;

abstract class AbstractSubscriber implements EventSubscriberInterface
{

}