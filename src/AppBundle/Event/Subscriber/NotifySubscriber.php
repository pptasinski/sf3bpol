<?php
/**
 * Created by PhpStorm.
 * User: pptasinski
 * Date: 25.05.17
 * Time: 21:36
 */

namespace AppBundle\Event\Subscriber;


use AppBundle\Event\CardEvent;
use Monolog\Logger;

/**
 * Class NotifySubscriber
 * @package AppBundle\Event\Subscriber
 */
class NotifySubscriber extends AbstractSubscriber
{
    /**
     * @var Logger
     */
    private $logger;

    /**
     * NotifySubscriber constructor.
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            CardEvent::NAME => 'onCardSave',
        ];
    }

    /**
     * @param CardEvent $event
     */
    public function onCardSave(CardEvent $event)
    {

        if ($event->getCardTypeModel()) {
            $this->logger->addError(sprintf('Card registered %s', $event->getCardTypeModel()->getCardNumber()));
        }

    }
}