<?php
/**
 * Created by PhpStorm.
 * User: pptasinski
 * Date: 25.05.17
 * Time: 22:26
 */

namespace AppBundle\Event;


use AppBundle\Form\Model\CardTypeModel;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class CardEvent
 * @package AppBundle\Event
 */
class CardEvent extends Event
{
    const NAME = 'notify.card';

    protected $cardTypeModel;

    public function __construct(CardTypeModel $cardTypeModel)
    {
        $this->cardTypeModel = $cardTypeModel;
    }

    public function getCardTypeModel()
    {
        return $this->cardTypeModel;
    }
}