<?php
/**
 * Created by PhpStorm.
 * User: pptasinski
 * Date: 25.05.17
 * Time: 22:54
 */

namespace AppBundle\Command;


use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CancelSubscriptionCommand extends AbstractCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:cancel-subscriptions')
            ->setDescription('Cancels subscriptions');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cancelInterval = (int)$this->getContainer()->getParameter('cancel_interval') + (int)$this->getContainer()->getParameter('month_length');

        $subscriptionsToCancel = $this->getEntityManager()->getRepository('AppBundle:Subscription')->findSubscriptionsToCancel($cancelInterval);
        $subscriptionsToCancelCount = count($subscriptionsToCancel);
        if ($subscriptionsToCancelCount > 0) {

            $this->getEntityManager()->getRepository('AppBundle:Subscription')->cancelSubscriptionsByIds($subscriptionsToCancel);

            $output->writeln(sprintf('Canceled %d subscriptions', $subscriptionsToCancelCount));
        }


    }

}