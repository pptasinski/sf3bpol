<?php

namespace AppBundle\Command;

use AppBundle\Entity\Subscription;
use AppBundle\Entity\SubscriptionPayment;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InstallCommand extends AbstractCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:install')
            ->setDescription('Adds subscriptions and subscriptions payments');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getEntityManager();

        $subscription1 = new Subscription();
        $subscription1
            ->setUser(1)
            ->setSubscriptionShippingAddress(1)
            ->setSubscriptionBillingAddress(1)
            ->setStatus(Subscription::STATUS_NEW)
            ->setSubscriptionPack(5)
            ->setCreatedAt(new \DateTime());


        $em->persist($subscription1);

        $subscription2 = new Subscription();
        $subscription2
            ->setUser(2)
            ->setSubscriptionShippingAddress(2)
            ->setSubscriptionBillingAddress(2)
            ->setStatus(Subscription::STATUS_ACTIVE)
            ->setSubscriptionPack(2)
            ->setStartedAt(new \DateTime('2017-04-01'))
            ->setCreatedAt(new \DateTime());


        $em->persist($subscription2);

        $subscription3 = new Subscription();
        $subscription3
            ->setUser(3)
            ->setSubscriptionShippingAddress(3)
            ->setSubscriptionBillingAddress(3)
            ->setStatus(Subscription::STATUS_ACTIVE)
            ->setSubscriptionPack(2)
            ->setStartedAt(new \DateTime('2017-04-15'))
            ->setCreatedAt(new \DateTime());
        $em->persist($subscription3);


        $subscriptionPayment1 = new SubscriptionPayment();

        $subscriptionPayment1
            ->setSubscription($subscription2)
            ->setChargedAmount(2400)
            ->setDate(new \DateTime('2017-04-01'))
            ->setCreatedAt(new \DateTime());

        $em->persist($subscriptionPayment1);

        $subscriptionPayment2 = new SubscriptionPayment();

        $subscriptionPayment2
            ->setSubscription($subscription2)
            ->setChargedAmount(1700)
            ->setDate(new \DateTime('2017-05-01'))
            ->setCreatedAt(new \DateTime());

        $em->persist($subscriptionPayment2);

        $subscriptionPayment3 = new SubscriptionPayment();

        $subscriptionPayment3
            ->setSubscription($subscription3)
            ->setChargedAmount(3600)
            ->setDate(new \DateTime('2017-04-15'))
            ->setCreatedAt(new \DateTime());

        $em->persist($subscriptionPayment3);

        $em->flush();

    }
}
