<?php
/**
 * Created by PhpStorm.
 * User: pptasinski
 * Date: 25.05.17
 * Time: 23:04
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Subscription;
use Doctrine\ORM\EntityRepository;

class SubscriptionRepository extends EntityRepository
{
    public function findSubscriptionsToCancel($dayInterval = 37)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $dateTime = new \DateTime(sprintf('-%dday', $dayInterval));
        $dateTime->setTime(0, 0, 0);

        $qb
            ->from('AppBundle:Subscription', 'subscription')
            ->join('subscription.subscriptionPayments', 'subscription_payments')
            ->where('subscription.status=:status')
            ->having('max(subscription_payments.date)<=:date')
            ->select('subscription.id')
            ->groupBy('subscription.id')
            ->setParameter('status', Subscription::STATUS_ACTIVE)
            ->setParameter('date', $dateTime);

        return $qb->getQuery()->getResult();

    }

    public function cancelSubscriptionsByIds(array $ids)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->update('AppBundle:Subscription', 'subscription')
            ->set('subscription.status', ':status')
            ->setParameter('status', Subscription::STATUS_CANCELED)
            ->where('subscription.id in (:ids)')
            ->setParameter('ids', $ids);

        return $qb->getQuery()->execute();
    }

}