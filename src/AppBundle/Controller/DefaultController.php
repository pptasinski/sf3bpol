<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Subscription;
use AppBundle\Event\CardEvent;
use AppBundle\Form\CardType;
use AppBundle\Form\Model\CardTypeModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {

        $card = new CardTypeModel();

        $form = $this->createForm(CardType::class, $card);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $subscription = $em->getRepository('AppBundle:Subscription')->find(1);

            if ($subscription instanceof Subscription) {

                $subscription
                    ->setStatus(Subscription::STATUS_ACTIVE);

                $em->persist($subscription);
                $em->flush();

                $this->get('event_dispatcher')->dispatch(CardEvent::NAME, new CardEvent($card));

            }

            return $this->redirectToRoute('homepage');

        }

        return $this->render('default/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/subscriptions/{id}", name="subscriptions_list")
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function subscriptionsListAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $subscriptions = $em->getRepository('AppBundle:Subscription')->findBy([
            'user' => $id,
        ]);

        return $this->render('default/subscriptions.html.twig', [
            'subscriptions' => $subscriptions,
        ]);
    }

}
