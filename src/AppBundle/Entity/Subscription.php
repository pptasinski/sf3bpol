<?php
/**
 * Created by PhpStorm.
 * User: pptasinski
 * Date: 25.05.17
 * Time: 21:08
 */

namespace AppBundle\Entity;


use AppBundle\Model\Common\TimestampableTrait;
use AppBundle\Model\TimestampableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * Keeps monthly renewed subscriptions
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubscriptionRepository")
 * @ORM\Table(name="subscription")
 */
class Subscription implements TimestampableInterface
{

    use TimestampableTrait;

    /**
     * @const
     */
    const STATUS_NEW = 'new';

    /**
     * @const
     */
    const STATUS_ACTIVE = 'active';

    /**
     * @const
     */
    const STATUS_CANCELED = 'canceled';

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", name="user_id", nullable=false)
     */
    private $user;

    /**
     * @var int|null
     * @ORM\Column(type="integer", name="subscription_shipping_address_id", nullable=true)
     */
    private $subscriptionShippingAddress;

    /**
     * @var int|null
     * @ORM\Column(type="integer", name="subscription_billing_address_id", nullable=true)
     */
    private $subscriptionBillingAddress;

    /**
     * @var string
     * @ORM\Column(type="string", length=16)
     */
    private $status = self::STATUS_NEW;

    /**
     * @var int
     * @ORM\Column(type="integer", name=" subscription_pack_id", nullable=false)
     */
    private $subscriptionPack;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startedAt;

    /**
     * @var SubscriptionPayment[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SubscriptionPayment", mappedBy="subscription", orphanRemoval=true, cascade={"persist"})
     */
    private $subscriptionPayments;

    public function __construct()
    {
        $this->subscriptionPayments = new ArrayCollection();
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [self::STATUS_ACTIVE, self::STATUS_NEW, self::STATUS_CANCELED];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Subscription
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int $user
     * @return Subscription
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSubscriptionShippingAddress()
    {
        return $this->subscriptionShippingAddress;
    }

    /**
     * @param int|null $subscriptionShippingAddress
     * @return Subscription
     */
    public function setSubscriptionShippingAddress($subscriptionShippingAddress)
    {
        $this->subscriptionShippingAddress = $subscriptionShippingAddress;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSubscriptionBillingAddress()
    {
        return $this->subscriptionBillingAddress;
    }

    /**
     * @param int|null $subscriptionBillingAddress
     * @return Subscription
     */
    public function setSubscriptionBillingAddress($subscriptionBillingAddress)
    {
        $this->subscriptionBillingAddress = $subscriptionBillingAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Subscription
     */
    public function setStatus($status)
    {
        if ($status === self::STATUS_ACTIVE) {
            $this->startedAt = new \DateTime();
        }

        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getSubscriptionPack()
    {
        return $this->subscriptionPack;
    }

    /**
     * @param int $subscriptionPack
     * @return Subscription
     */
    public function setSubscriptionPack($subscriptionPack)
    {
        $this->subscriptionPack = $subscriptionPack;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * @param \DateTime|null $startedAt
     * @return Subscription
     */
    public function setStartedAt($startedAt)
    {
        $this->startedAt = $startedAt;
        return $this;
    }

    /**
     * @return SubscriptionPayment[]|ArrayCollection
     */
    public function getSubscriptionPayments()
    {
        return $this->subscriptionPayments;
    }

    /**
     * @param SubscriptionPayment[]|ArrayCollection $subscriptionPayments
     * @return Subscription
     */
    public function setSubscriptionPayments($subscriptionPayments)
    {
        $this->subscriptionPayments = $subscriptionPayments;
        return $this;
    }

    /**
     * @param SubscriptionPayment $payment
     * @return $this
     */
    public function addSubscriptionPayment(SubscriptionPayment $payment)
    {

        if (!$this->subscriptionPayments->contains($payment)) {
            $this->subscriptionPayments->add($payment);
        }

        return $this;

    }


}
