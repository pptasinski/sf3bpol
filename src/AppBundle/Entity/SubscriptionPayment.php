<?php
/**
 * Created by PhpStorm.
 * User: pptasinski
 * Date: 25.05.17
 * Time: 21:09
 */

namespace AppBundle\Entity;


use AppBundle\Model\Common\TimestampableTrait;
use AppBundle\Model\TimestampableInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="subscription_payment")
 */
class SubscriptionPayment implements TimestampableInterface
{

    use TimestampableTrait;

    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Subscription|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Subscription", inversedBy="subscriptionPayments")
     * @ORM\JoinColumn(name="subscription_id")
     */
    private $subscription;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $chargedAmount;

    /**
     * TODO: Change column name because it is mysql keyword
     * @var \DateTime
     * @ORM\Column(type="date", nullable=false)
     */
    private $date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return SubscriptionPayment
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param mixed $subscription
     * @return SubscriptionPayment
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChargedAmount()
    {
        return $this->chargedAmount;
    }

    /**
     * @param mixed $chargedAmount
     * @return SubscriptionPayment
     */
    public function setChargedAmount($chargedAmount)
    {
        $this->chargedAmount = $chargedAmount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return SubscriptionPayment
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

}