<?php
/**
 * Created by PhpStorm.
 * User: pptasinski
 * Date: 25.05.17
 * Time: 21:12
 */

namespace AppBundle\Model;

/**
 * TODO: Doctrine filter to catch persist/update and update fields
 * Interface TimestampableInterface
 * @package AppBundle\Model
 */
interface TimestampableInterface
{
    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt();

    /**
     * @param \DateTime|null $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @param \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);
}