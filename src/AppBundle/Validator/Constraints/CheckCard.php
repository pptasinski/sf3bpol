<?php
/**
 * Created by PhpStorm.
 * User: pptasinski
 * Date: 25.05.17
 * Time: 21:38
 */

namespace AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * Class CheckCard
 * @package AppBundle\Validator\Constraints
 * @Annotation
 */
class CheckCard extends Constraint
{

    public $message = 'Given card number {{ value }} is wrong!';

    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}