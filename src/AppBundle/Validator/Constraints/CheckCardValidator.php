<?php
/**
 * Created by PhpStorm.
 * User: pptasinski
 * Date: 25.05.17
 * Time: 21:39
 */

namespace AppBundle\Validator\Constraints;


use AppBundle\Form\Model\CardTypeModel;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\CardScheme;
use Symfony\Component\Validator\Constraints\CardSchemeValidator;

class CheckCardValidator extends CardSchemeValidator
{

    public function validate($value, Constraint $constraint)
    {

        /**
         * @var CardTypeModel $value
         */
        if ($value instanceof CardTypeModel) {


            if (null === $value->getCardNumber() || '' === $value->getCardNumber()) {
                return;
            }

            if (!is_numeric($value->getCardNumber())) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $this->formatValue($value->getCardNumber()))
                    ->setCode(CardScheme::NOT_NUMERIC_ERROR)
                    ->addViolation();

                return;
            }

            if (isset($this->schemes[$value->getCardType()])) {

                foreach ($this->schemes[$value->getCardType()] as $regex) {
                    if (preg_match($regex, $value->getCardNumber())) {
                        return;
                    }
                }
            }

            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value->getCardNumber()))
                ->setCode(CardScheme::INVALID_FORMAT_ERROR)
                ->addViolation();

        } else {
            throw new \InvalidArgumentException('Bad object passed to CheckCardValidator');
        }

    }

}