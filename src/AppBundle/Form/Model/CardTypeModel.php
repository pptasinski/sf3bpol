<?php
/**
 * Created by PhpStorm.
 * User: pptasinski
 * Date: 25.05.17
 * Time: 21:28
 */

namespace AppBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppAssert;

/**
 * Class CardTypeModel
 * @package AppBundle\Form\Model
 * @AppAssert\CheckCard()
 */
class CardTypeModel
{
    /**
     * @const array (assuming php>=5.6)
     */
    const CARD_TYPES = [
        'MasterCard' => 'MASTERCARD',
        'Visa' => 'VISA',
        'AmericanExpress' => 'AMEX',
    ];

    /**
     * @Assert\NotNull()
     * @var string
     */
    private $cardNumber;

    /**
     *
     * @Assert\NotNull()
     * @Assert\Type(type="numeric")
     * @Assert\Length(min=3, max=3)
     * @var string
     */
    private $cvvNumber;

    /**
     *
     * @Assert\Choice(callback="getCardTypes")
     * @Assert\NotNull()
     * @var string
     */
    private $cardType;

    /**
     * @return array
     */
    public static function getCardTypes()
    {
        return self::CARD_TYPES;
    }

    /**
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     * @return CardTypeModel
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getCvvNumber()
    {
        return $this->cvvNumber;
    }

    /**
     * @param string $cvvNumber
     * @return CardTypeModel
     */
    public function setCvvNumber($cvvNumber)
    {
        $this->cvvNumber = $cvvNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @param string $cardType
     * @return CardTypeModel
     */
    public function setCardType($cardType)
    {
        $this->cardType = $cardType;
        return $this;
    }

}